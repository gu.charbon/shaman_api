"""
This module defines the connection to IOI mongodb database as well as
all functions that interact with this database.
"""
from typing import Optional
from pymongo import MongoClient
from pymongo.cursor import Cursor
from ..models import Experiment, IntermediateResult, FinalResult
from ..config import CONFIG
from ..logger import get_logger


logger = get_logger(__name__)
shaman_db = None


def connect_shaman_db(**kwargs):
    global shaman_db
    host = CONFIG.shaman_mongodb_host
    port = CONFIG.shaman_mongodb_port
    database = CONFIG.shaman_mongodb_database
    logger.debug(
        f"Connecting to SHAMAN mongodb database with uri: mngodb://{host}:{port}/{database}"
    )
    shaman_client = MongoClient(host=host, port=port)
    shaman_db = shaman_client[CONFIG.shaman_mongodb_database]


def close_shaman_db():
    logger.debug("Closing connections to SHAMAN mongodb database")
    shaman_db.client.close()


def create_experiment(experiment: Experiment):
    """
    Experiments are created inside shaman mongodb database
    """
    shaman_db["experiments"].insert_one(experiment)


def get_experiments(limit: Optional[int] = None) -> Cursor:
    """
    Return a cursor of all experiments.
    Optionaly you can give a limit argument to return a limited number of experiments
    """
    all_experiments = shaman_db["experiments"].find()
    if limit:
        all_experiments = all_experiments.limit(limit)
    return all_experiments


def get_experiment(experiment_id: str) -> Experiment:
    """
    Return a single experiment based on given experiment ID
    Return None if the experiment do not exist.
    """
    experiment = shaman_db["experiments"].find_one({"experiment_id": experiment_id})
    return experiment


def update_experiment(experiment_id: str, result: IntermediateResult):
    """
    Experiments are updated inside shaman mongodb database
    """
    # TODO: I don't think you want to insert a new document here ?
    shaman_db["experiments"].insert_one(result)


def close_experiment(experiment_id: str, final_result: FinalResult):
    """
    Experiments are terminated inside shaman mongodb database
    """
    # TODO: I don't think you want to insert a new document here ?
    shaman_db["experiments"].insert_one(final_result)
