"""
Databases subpackage of shaman_api package.
This subpackages defines connections to mongodb databases
and functions to interact with databases
"""
from .ioi import connect_ioi_db, close_ioi_db
from .shaman import (
    connect_shaman_db,
    close_shaman_db,
    create_experiment,
    get_experiments,
    get_experiment,
    update_experiment,
    close_experiment,
)

__all__ = [
    "connect_shaman_db",
    "close_shaman_db",
    "create_experiment",
    "get_experiments",
    "get_experiment",
    "update_experiment",
    "close_experiment",
    "connect_ioi_db",
    "close_ioi_db",
]
