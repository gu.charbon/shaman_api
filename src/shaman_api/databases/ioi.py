"""
This module defines the connection to IOI mongodb database as well as
all functions that interact with this database.
"""
from pymongo import MongoClient
from ..config import CONFIG
from ..logger import get_logger


logger = get_logger(__name__)
ioi_db = None


def connect_ioi_db(**kwargs):
    global ioi_db
    host = CONFIG.ioi_mongodb_host
    port = CONFIG.ioi_mongodb_port
    database = CONFIG.ioi_mongodb_database
    logger.debug(
        f"Connecting to ioi mongodb database with uri: mngodb://{host}:{port}/{database}"
    )
    ioi_client = MongoClient(host=host, port=port)
    ioi_db = ioi_client[CONFIG.ioi_mongodb_database]


def close_ioi_db():
    logger.debug("Closing connections to IOI mongodb database")
    ioi_db.client.close()
