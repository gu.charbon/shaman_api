from fastapi import FastAPI
from .routers import experiment_router, ioi_router


app = FastAPI(
    title="SHAMan API",
    version="1.0.2",
    description="A REST API to interact with SHAMan Optimization Engine",
)


app.include_router(ioi_router)
app.include_router(experiment_router, prefix="/experiments", tags=["Experiments"])
