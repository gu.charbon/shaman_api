"""
Rest API endpoints related to IOI are defined in this module
"""
from fastapi import APIRouter
from ..databases import connect_ioi_db, close_ioi_db
from ..logger import get_logger


logger = get_logger(__name__)


router = APIRouter(on_startup=[connect_ioi_db], on_shutdown=[close_ioi_db])
