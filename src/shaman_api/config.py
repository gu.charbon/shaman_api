"""
configuration module of datasets package
"""
from enum import Enum
from pydantic import BaseSettings


class LoggingLevel(str, Enum):
    """
    Allowed log levels for the application
    """

    CRITICAL: str = "CRITICAL"
    ERROR: str = "ERROR"
    WARNING: str = "WARNING"
    INFO: str = "INFO"
    DEBUG: str = "DEBUG"


class AppConfig(BaseSettings):
    """
    Configuration can be set using:
        - Keyword arguments at instantation
        - Using environment variables
    """

    shaman_mongodb_host: str = "127.0.0.1"
    shaman_mongodb_port: int = 27018
    shaman_mongodb_database: str = "shaman_db"
    ioi_mongodb_host: str = "127.0.0.1"
    ioi_mongodb_port: int = 27017
    ioi_mongodb_database: str = "cmd_database"
    log_level: LoggingLevel = "DEBUG"

    class Config:
        case_sensitive = False


CONFIG = AppConfig()
