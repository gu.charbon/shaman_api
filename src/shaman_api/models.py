from pydantic import BaseModel
from typing import List, Dict


class Experiment(BaseModel):
    """
    A model describing an experiment.
    """

    experiment_id: str
    jobids: List[int]
    experiment_name: str


class IntermediateResult(BaseModel):
    """
    A model describing an intermediate result of an experiment
    """

    perf: float
    parameters: Dict[str, float]


class FinalResult(BaseModel):
    """
    A model describing final result of an experiment.
    """

    best_parameters: Dict[str, float]
    best_perf: float


class Message(BaseModel):
    detail: str
